package com.hzukhruf.crudapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzukhruf.crudapi.configuration.Response;
import com.hzukhruf.crudapi.model.dto.PositionDto;
import com.hzukhruf.crudapi.model.entity.Position;
import com.hzukhruf.crudapi.repository.PositionRepository;
import com.hzukhruf.crudapi.service.PositionService;

@RestController
@RequestMapping("/api/position")
public class PositionController {

	@Autowired
	private PositionService service;
	@Autowired
	private PositionRepository repository;

	@PostMapping
	public Response<PositionDto> insert(@RequestBody PositionDto dto) {
		Position position = convertToEntity(dto);
		return Response.ok(convertToDto(service.insert(position)));
	}

	@GetMapping
	public Response<List<String>> getPosition() {
		List<String> positionName = repository.getPosition();
		return Response.ok(positionName);
	}

	@PutMapping("/{id}")
	public Response<PositionDto> update(@PathVariable Integer id, @RequestBody PositionDto dto) {
		if (repository.findById(id).isPresent()) {
			Position position = repository.findById(id).get();
			if (dto.getCode() != null)
				position.setCode(dto.getCode());
			if (dto.getName() != null)
				position.setName(dto.getName());
			return Response.ok(convertToDto(service.update(position)));
		}
		return Response.error("Data tidak ditemukan");
	}

	@DeleteMapping("/{id}")
	public Response<PositionDto> delete(@PathVariable Integer id) {
		if (repository.findById(id).isPresent()) {
			return Response.ok(convertToDto(service.delete(id)));
		}
		return Response.error("Data tidak ditemukan");
	}

	private Position convertToEntity(PositionDto dto) {
		Position position = new Position();
		position.setId(dto.getId());
		position.setName(dto.getName());
		position.setCode(dto.getCode());
		position.setIsDelete(dto.getIsDelete());
		return position;
	}

	private PositionDto convertToDto(Position entity) {
		PositionDto positionDto = new PositionDto();
		positionDto.setId(entity.getId());
		positionDto.setCode(entity.getCode());
		positionDto.setName(entity.getName());
		positionDto.setIsDelete(entity.getIsDelete());
		return positionDto;
	}
}
