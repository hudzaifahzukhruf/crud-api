package com.hzukhruf.crudapi.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzukhruf.crudapi.configuration.Response;
import com.hzukhruf.crudapi.model.dto.EmployeDto;
import com.hzukhruf.crudapi.model.entity.Employee;
import com.hzukhruf.crudapi.repository.EmployeeRepository;
import com.hzukhruf.crudapi.repository.PositionRepository;
import com.hzukhruf.crudapi.service.EmployeeService;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {
	@Autowired
	private EmployeeRepository repository;
	@Autowired
	private PositionRepository positionRepository;
	@Autowired
	private EmployeeService service;

	@PostMapping
	public Response<EmployeDto> insert(@RequestBody EmployeDto dto) {
		Employee employee = convertToEntity(dto);
		return Response.ok(convertToDto(service.insert(employee)));
	}

	@GetMapping
	public Response<List<EmployeDto>> getEmployee() {
		List<Employee> employees = repository.getEmployees();
		List<EmployeDto> employeDtos = employees.stream().map(employee -> convertToDto(employee))
				.collect(Collectors.toList());
		return Response.ok(employeDtos);
	}

	@GetMapping("/nip")
	public Response<List<Integer>> getNip() {
		List<Integer> nip = repository.getIdNumber();
		return Response.ok(nip);
	}

	@GetMapping("/id/{id}")
	public Response<EmployeDto> getEmployeeById(@PathVariable Integer id) {
		Employee employees = repository.findById(id).get();
		return Response.ok(convertToDto(employees));
	}

	@PutMapping("/{id}")
	public Response<EmployeDto> update(@PathVariable Integer id, @RequestBody EmployeDto dto) {
		if (repository.findById(id).isPresent()) {
			Employee employee = repository.findById(id).get();
			if (dto.getName() != null)
				employee.setName(dto.getName());
			if (dto.getBirthDate() != null)
				employee.setBirthDate(dto.getBirthDate());
			if (dto.getIdNumber() != null)
				employee.setIdNumber(dto.getIdNumber());
			if (dto.getPosition() != null)
				employee.setPosition(positionRepository.findByName(dto.getPosition()));
			if (dto.getGender() != null)
				if (dto.getGender() == '1') {
					employee.setGender(1);
				} else {
					employee.setGender(2);
				}

			return Response.ok(convertToDto(service.update(employee)));
		}
		return Response.error("Data tidak ditemukan");
	}

	@DeleteMapping("/{id}")
	public Response<EmployeDto> delete(@PathVariable Integer id) {
		if (repository.findById(id).isPresent()) {
			return Response.ok(convertToDto(service.delete(id)));
		}
		return Response.error("Data tidak ditemukan");
	}

	private Employee convertToEntity(EmployeDto dto) {
		Employee employee = new Employee();
		employee.setId(dto.getId());
		employee.setName(dto.getName());
		employee.setBirthDate(dto.getBirthDate());
		employee.setIdNumber(dto.getIdNumber());
		// System.out.println(dto.getGender()=="Wanita");
		if (dto.getGender() == '1') {
			employee.setGender(1);
			System.out.println("true");
		} else {
			employee.setGender(2);
			System.out.println("false");
		}

		employee.setIsDelete(dto.getIsDelete());
		employee.setPosition(positionRepository.findByName(dto.getPosition()));
		return employee;
	}

	private EmployeDto convertToDto(Employee employee) {
		EmployeDto employeDto = new EmployeDto();
		employeDto.setId(employee.getId());
		employeDto.setName(employee.getName());
		employeDto.setBirthDate(employee.getBirthDate());
		employeDto.setPosition(employee.getPosition().getName());
		employeDto.setIdNumber(employee.getIdNumber());
		if (employee.getGender() == 1) {
			employeDto.setGender('1');
		} else {
			employeDto.setGender('2');
		}

		employeDto.setIsDelete(employee.getIsDelete());
		return employeDto;
	}

}
