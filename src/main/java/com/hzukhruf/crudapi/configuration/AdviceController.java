package com.hzukhruf.crudapi.configuration;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class AdviceController {

	@ExceptionHandler(Exception.class)
	public @ResponseBody Response<?> handleException(Exception e) {
		return Response.error(e.getMessage());
	}

}
