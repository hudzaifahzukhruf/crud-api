package com.hzukhruf.crudapi.configuration;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class Response<T> {
    private int status;
    private String message;
    private T data;

    public Response(int status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public static <T> Response<T> ok(T data) {
        return new Response<T>(HttpStatus.OK.value(), "SUCCESS", data);
    }

    public static <T> Response<T> ok(T data, String message) {
        return new Response<T>(HttpStatus.OK.value(), message, data);
    }

    public static <T> Response<T> error(String message) {
        return new Response<T>(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
    }
}
