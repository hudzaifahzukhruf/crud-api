package com.hzukhruf.crudapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hzukhruf.crudapi.model.entity.Position;

@Repository
public interface PositionRepository extends JpaRepository<Position, Integer> {
	Position findByName(String name);
	// @Query(value = "SELECT * FROM t1_position WHERE name=?1 AND is_delete=0",
	// nativeQuery = true)
	// Position findByName(String name);

	@Query(value = "SELECT name FROM t1_position WHERE is_delete=0", nativeQuery = true)
	List<String> getPosition();
}
