package com.hzukhruf.crudapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hzukhruf.crudapi.model.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	@Query(value = "SELECT * FROM t2_employee WHERE is_delete=0", nativeQuery = true)
	List<Employee> getEmployees();

	@Query(value = "SELECT id_number FROM t2_employee WHERE is_delete=0", nativeQuery = true)
	List<Integer> getIdNumber();

}
