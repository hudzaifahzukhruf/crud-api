package com.hzukhruf.crudapi.model.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "t2_employee")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11, nullable = false)
	private Integer id;

	@Column(name = "name", length = 100, nullable = false)
	private String name;

	@Column(name = "birth_date", nullable = false)
	private Date birthDate;

	@ManyToOne
	@JoinColumn(name = "position_id", nullable = false)
	private Position position;

	@Column(name = "id_number", length = 11, nullable = false, unique = true)
	private Integer idNumber;

	@Column(name = "gender", length = 11, nullable = false)
	private Integer gender;

	@Column(name = "is_delete", length = 11, nullable = false)
	private Integer isDelete;

}
