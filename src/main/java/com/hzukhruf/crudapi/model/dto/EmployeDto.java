package com.hzukhruf.crudapi.model.dto;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeDto {
	private Integer id;
	private String name;
	private Date birthDate;
	private String position;
	private Integer idNumber;
	private Character gender;
	private Integer isDelete;

}
