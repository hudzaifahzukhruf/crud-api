package com.hzukhruf.crudapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PositionDto {
	private Integer id;
	private String code;
	private String name;
	private Integer isDelete;
}
