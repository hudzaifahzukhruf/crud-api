package com.hzukhruf.crudapi.service;

import com.hzukhruf.crudapi.model.entity.Position;

public interface PositionService {
	Position insert(Position position);

	Position update(Position position);

	Position delete(Integer id);

}
