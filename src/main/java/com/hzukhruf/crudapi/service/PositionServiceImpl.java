package com.hzukhruf.crudapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hzukhruf.crudapi.model.entity.Position;
import com.hzukhruf.crudapi.repository.PositionRepository;

@Service
@Transactional
public class PositionServiceImpl implements PositionService {
	@Autowired
	private PositionRepository repository;

	@Override
	public Position insert(Position position) {
		position.setIsDelete(0);
		return repository.save(position);
	}

	@Override
	public Position update(Position position) {
		// Position entity = repository.save(position);
		position.setIsDelete(0);
		return repository.save(position);
	}

	@Override
	public Position delete(Integer id) {
		Position entity = repository.findById(id).get();
		entity.setIsDelete(1);
		return repository.save(entity);
	}

}
