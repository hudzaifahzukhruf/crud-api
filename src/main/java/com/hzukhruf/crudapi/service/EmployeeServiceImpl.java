package com.hzukhruf.crudapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hzukhruf.crudapi.model.entity.Employee;
import com.hzukhruf.crudapi.repository.EmployeeRepository;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private EmployeeRepository repository;

	@Override
	public Employee insert(Employee employee) {
		// Employee entity = repository.save(employee);
		employee.setIsDelete(0);
		return repository.save(employee);
	}

	@Override
	public Employee update(Employee employee) {
		employee.setIsDelete(0);
		return repository.save(employee);
	}

	@Override
	public Employee delete(Integer id) {
		Employee entity = repository.findById(id).get();
		entity.setIsDelete(1);
		return repository.save(entity);
	}

}
