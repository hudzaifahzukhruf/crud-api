package com.hzukhruf.crudapi.service;

import com.hzukhruf.crudapi.model.entity.Employee;

public interface EmployeeService {
	Employee insert(Employee employee);

	Employee update(Employee employee);

	Employee delete(Integer id);

}
